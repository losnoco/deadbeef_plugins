#!/bin/sh

pushd $(dirname $0)
BASE=`pwd -P`
popd

BUILDPRODUCTS="$BASE"/build/Build/Products/Release

for f in "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources/*.dylib;
do
	codesign -s 'Developer ID Application' "$f";
done

codesign -s 'Developer ID Application' --force "$BUILDPRODUCTS"/deadbeef.app

ditto -c -k --sequesterRsrc --keepParent --zlibCompressionLevel 9 "$BUILDPRODUCTS"/deadbeef.app "$BASE"/deadbeef_osx.zip
 
