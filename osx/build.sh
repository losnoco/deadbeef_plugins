#!/bin/sh

pushd $(dirname $0)
BASE=`pwd -P`
popd

BUILDPRODUCTS="$BASE"/build/Build/Products/Release

xcodebuild -workspace "$BASE"/deadbeef.xcworkspace -scheme deadbeef -configuration Deployment -derivedDataPath "$BASE"/build

xcodebuild -workspace "$BASE"/deadbeef.xcworkspace -scheme xsf -configuration Deployment -derivedDataPath "$BASE"/build
xcodebuild -workspace "$BASE"/deadbeef.xcworkspace -scheme vfs_fex -configuration Deployment -derivedDataPath "$BASE"/build
xcodebuild -workspace "$BASE"/deadbeef.xcworkspace -scheme midi -configuration Deployment -derivedDataPath "$BASE"/build
xcodebuild -workspace "$BASE"/deadbeef.xcworkspace -scheme vgm -configuration Deployment -derivedDataPath "$BASE"/build

rm -f "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources/vfs_zip.dylib
rm -f "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources/psf.dylib
rm -f "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources/wildmidi.dylib
rm -f "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources/wma.dylib
rm -f "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources/dca.dylib

cp -a "$BUILDPRODUCTS"/xsf.dylib "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources
cp -a "$BUILDPRODUCTS"/vfs_fex.dylib "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources
cp -a "$BUILDPRODUCTS"/midi.dylib "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources
cp -a "$BUILDPRODUCTS"/vgm.dylib "$BUILDPRODUCTS"/deadbeef.app/Contents/Resources
