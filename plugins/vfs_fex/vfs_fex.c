/*
    File_Extractor VFS plugin for DeaDBeeF Player
    Copyright (C) 2009-2014 Alexey Yakovenko
    Copyright (C) 2015-2024 Christopher Snowhill

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <deadbeef/deadbeef.h>

#include <fex/fex.h>

#define trace(...) { fprintf(stderr, __VA_ARGS__); }
//#define trace(fmt,...)

#define min(x,y) ((x)<(y)?(x):(y))

static DB_functions_t *deadbeef;
static DB_vfs_t plugin;

typedef struct {
    DB_FILE file;
    fex_t* ex;
    const uint8_t* data;
    int64_t offset;
    int64_t size;
} ddb_fex_file_t;

static const char *scheme_names[] = { "fex://", NULL };

static int vfs_fex_start(void) {
    fex_init();
    return 0;
}

const char **
vfs_fex_get_schemes (void) {
    return scheme_names;
}

int
vfs_fex_is_streaming (void) {
    return 0;
}

// fname must have form of zip://full_filepath.zip:full_filepath_in_zip
DB_FILE*
vfs_fex_open (const char *fname) {
    trace ("vfs_fex: open %s\n", fname);
    if (strncasecmp (fname, "fex://", 6)) {
        return NULL;
    }

    fname += 6;
    const char *colon = strchr (fname, ':');
    if (!colon) {
        return NULL;
    }


    char fexname[colon-fname+1];
    memcpy (fexname, fname, colon-fname);
    fexname[colon-fname] = 0;

    fname = colon+1;

    fex_t *ex;
    fex_err_t err = fex_open( &ex, fexname );
    if (err) {
        trace( "vfs_fex: open error %s\n", err );
        return NULL;
    }
    
    while (!fex_done(ex)) {
        err = fex_stat(ex);
        if (err) {
            trace( "vfs_fex: stat error %s\n", err );
            fex_close(ex);
            return NULL;
        }
        if (!strcmp(fname, fex_name(ex)))
            break;
        err = fex_next(ex);
        if (err) {
            trace( "vfs_fex: next error %s\n", err );
            fex_close(ex);
            return NULL;
        }
    }
    
    if (fex_done(ex)) {
        trace( "vfs_fex: file not found in archive\n" );
        fex_close(ex);
        return NULL;
    }

    ddb_fex_file_t *f = malloc (sizeof (ddb_fex_file_t));
    memset (f, 0, sizeof (ddb_fex_file_t));
    f->file.vfs = &plugin;
    f->ex = ex;
    f->size = fex_size(ex);
    err = fex_data(ex, (const void **) &f->data);
    if (err) {
        trace( "vfs_fex: data error %s\n", err );
        fex_close(ex);
        free(f);
        return NULL;
    }
    trace ("vfs_fex: end open %s\n", fname);
    return (DB_FILE*)f;
}

void
vfs_fex_close (DB_FILE *f) {
    trace ("vfs_fex: close\n");
    ddb_fex_file_t *ff = (ddb_fex_file_t *)f;
    if (ff->ex) {
        fex_close(ff->ex);
    }
    free (ff);
}

size_t
vfs_fex_read (void *ptr, size_t size, size_t nmemb, DB_FILE *f) {
    ddb_fex_file_t *ff = (ddb_fex_file_t *)f;
//    printf ("read: %d\n", size*nmemb);

    size_t sz = size * nmemb;
    
    if (sz + ff->offset > ff->size)
        sz = ff->size - ff->offset;
    
    memcpy(ptr, ff->data + ff->offset, sz);
    ff->offset += sz;

    return sz / size;
}

int
vfs_fex_seek (DB_FILE *f, int64_t offset, int whence) {
    ddb_fex_file_t *ff = (ddb_fex_file_t *)f;
//    printf ("seek: %lld (%d)\n", offset, whence);

    if (whence == SEEK_CUR) {
        offset = ff->offset + offset;
    }
    else if (whence == SEEK_END) {
        offset = ff->size + offset;
    }

    if (offset > ff->size) {
        return -1;
    }

    ff->offset = offset;
    
    return 0;
}

int64_t
vfs_fex_tell (DB_FILE *f) {
    ddb_fex_file_t *ff = (ddb_fex_file_t *)f;
    return ff->offset;
}

void
vfs_fex_rewind (DB_FILE *f) {
    ddb_fex_file_t *ff = (ddb_fex_file_t *)f;
    ff->offset = 0;
}

int64_t
vfs_fex_getlength (DB_FILE *f) {
    ddb_fex_file_t *ff = (ddb_fex_file_t *)f;
    return ff->size;
}

int
vfs_fex_scandir (const char *dir, struct dirent ***namelist, int (*selector) (const struct dirent *), int (*cmp) (const struct dirent **, const struct dirent **)) {
    trace ("vfs_fex_scandir: %s\n", dir);
    fex_t * ex;
    fex_err_t err = fex_open( &ex, dir );
    if (err) {
        trace ("fex_open failed (%s)\n", err);
        return -1;
    }

    int num_files = 0;
    int num_guessed_files = 16;
    *namelist = malloc(sizeof(void *) * num_guessed_files);
    while (!fex_done(ex)) {
        err = fex_stat(ex);
        if (err) {
            trace ( "fex_stat failed (%s)\n", err );
            fex_close(ex);
            return -1;
        }
        struct dirent entry;
        strncpy(entry.d_name, fex_name(ex), sizeof(entry.d_name)-1);
        entry.d_name[sizeof(entry.d_name)-1] = '\0';
        if (!selector || (selector && selector(&entry))) {
            if (num_files >= num_guessed_files) {
                num_guessed_files += 16;
                *namelist = realloc(*namelist, sizeof(void *) * num_guessed_files);
            }
            (*namelist)[num_files] = calloc(1, sizeof(struct dirent));
            strcpy((*namelist)[num_files]->d_name, entry.d_name);
            num_files++;
            trace("vfs_fex: %s\n", entry.d_name);
        }
        err = fex_next(ex);
        if (err) {
            trace( "fex_next failed (%s)\n", err );
            fex_close(ex);
            return -1;
        }
    }
    *namelist = realloc(*namelist, sizeof(void *) * num_files);

    fex_close(ex);
    trace ("vfs_fex: scandir done\n");
    return num_files;
}

int
vfs_fex_is_container (const char *fname) {
    const char *ext = strrchr (fname, '.');
    const fex_type_t *type = fex_type_list();
    if (ext) {
        while (*type) {
            const char *cext = fex_type_extension(*type);
            if (*cext && !strcasecmp(ext, cext)) {
                return 1;
            }
            ++type;
        }
    }
    return 0;
}
const char *
vfs_fex_get_scheme_for_name (const char *fname) {
    return scheme_names[0];
}

static DB_vfs_t plugin = {
    .plugin.api_vmajor = 1,
    .plugin.api_vminor = 6,
    .plugin.version_major = 1,
    .plugin.version_minor = 0,
    .plugin.type = DB_PLUGIN_VFS,
    .plugin.id = "vfs_fex",
    .plugin.name = "File_Extractor vfs",
    .plugin.descr = "play files directly from zip/gz/rar/7z files",
    .plugin.copyright = 
        "File_Extractor VFS plugin for DeaDBeeF Player\n"
        "Copyright (C) 2009-2014 Alexey Yakovenko\n"
        "Copyright (C) 2015 Christopher Snowhill\n"
        "\n"
        "This software is provided 'as-is', without any express or implied\n"
        "warranty.  In no event will the authors be held liable for any damages\n"
        "arising from the use of this software.\n"
        "\n"
        "Permission is granted to anyone to use this software for any purpose,\n"
        "including commercial applications, and to alter it and redistribute it\n"
        "freely, subject to the following restrictions:\n"
        "\n"
        "1. The origin of this software must not be misrepresented; you must not\n"
        " claim that you wrote the original software. If you use this software\n"
        " in a product, an acknowledgment in the product documentation would be\n"
        " appreciated but is not required.\n"
        "\n"
        "2. Altered source versions must be plainly marked as such, and must not be\n"
        " misrepresented as being the original software.\n"
        "\n"
        "3. This notice may not be removed or altered from any source distribution.\n"
    ,
    .plugin.website = "https://gitlab.kode54.net",
    .plugin.start = vfs_fex_start,
    .open = vfs_fex_open,
    .close = vfs_fex_close,
    .read = vfs_fex_read,
    .seek = vfs_fex_seek,
    .tell = vfs_fex_tell,
    .rewind = vfs_fex_rewind,
    .getlength = vfs_fex_getlength,
    .get_schemes = vfs_fex_get_schemes,
    .is_streaming = vfs_fex_is_streaming,
    .is_container = vfs_fex_is_container,
    .scandir = vfs_fex_scandir,
    .get_scheme_for_name = vfs_fex_get_scheme_for_name,
};

__attribute__ ((visibility ("default")))
DB_plugin_t *
vfs_fex_load (DB_functions_t *api) {
    deadbeef = api;
    return DB_PLUGIN (&plugin);
}
