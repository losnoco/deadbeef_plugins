/*
    Midi plugin for DeaDBeeF Player
    Copyright (C) 2016-2024 Christopher Snowhill
 
    Based on WildMidi plugin for DeaDBeeF Player
    Copyright (C) 2009-2014 Alexey Yakovenko

    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/



#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <deadbeef/deadbeef.h>

#include "src/midi_processing/midi_processor.h"

#include "src/MIDIPlayer.h"
#include "src/MSPlayer.h"
#include "src/BMPlayer.h"

extern DB_decoder_t midi_plugin;

static int conf_driver = 0;
static int conf_patch = 3;
static int conf_fadeout = 10;
static int conf_loopcount = 2;
static int conf_play_forever = 0;

#define trace(...) { fprintf(stderr, __VA_ARGS__); }
//#define trace(fmt,...)

static DB_functions_t *deadbeef;

#define min(x,y) ((x)<(y)?(x):(y))
#define max(x,y) ((x)>(y)?(x):(y))

# define strdup(s)							      \
  (__extension__							      \
    ({									      \
      const char *__old = (s);						      \
      size_t __len = strlen (__old) + 1;				      \
      char *__new = (char *) malloc (__len);			      \
      (char *) memcpy (__new, __old, __len);				      \
    }))

typedef struct {
    DB_fileinfo_t info;
    char *path;
    midi_container *m;
    MIDIPlayer *p;
    float duration;
    float duration_fade;
    float duration_total;
    int can_loop;
    int is_looped;
} midi_info_t;

DB_fileinfo_t *
midi_open (uint32_t hints) {
    DB_fileinfo_t *_info = (DB_fileinfo_t *)malloc (sizeof (midi_info_t));
    memset (_info, 0, sizeof (midi_info_t));
    ((midi_info_t*)_info)->can_loop = hints & DDB_DECODER_HINT_CAN_LOOP;
    return _info;
}

static int
midi_open_file(midi_container *out, const char *uri);

static int
midi_open_file_for_decode(midi_info_t *info);

int
midi_init (DB_fileinfo_t *_info, DB_playItem_t *it) {
    midi_info_t *info = (midi_info_t *)_info;

    deadbeef->pl_lock ();
    info->path = strdup( deadbeef->pl_find_meta (it, ":URI") );
    deadbeef->pl_unlock ();
    
    if (midi_open_file_for_decode(info) < 0) {
        return -1;
    }

    _info->plugin = &midi_plugin;
    _info->fmt.channels = 2;
    _info->fmt.bps = 32;
    _info->fmt.is_float = true;
    _info->fmt.samplerate = 44100;
    _info->fmt.channelmask = _info->fmt.channels == 1 ? DDB_SPEAKER_FRONT_LEFT : (DDB_SPEAKER_FRONT_LEFT | DDB_SPEAKER_FRONT_RIGHT);
    _info->readpos = 0;

    return 0;
}

void
midi_free (DB_fileinfo_t *_info) {
    midi_info_t *info = (midi_info_t *)_info;
    if (info) {
        if (info->p) {
            delete info->p;
            info->p = NULL;
        }
        if (info->m) {
            delete info->m;
            info->m = NULL;
        }
        if (info->path) {
            free(info->path);
            info->path = NULL;
        }
        free (info);
    }
}

int
midi_read (DB_fileinfo_t *_info, char *bytes, int size) {
    midi_info_t *info = (midi_info_t *)_info;
    int frames = size / (sizeof(float) * 2);
    int playForever = conf_play_forever && info->can_loop;
    float seconds = frames/(float)_info->fmt.samplerate;
    float localduration = info->duration;;
    float localdurationtotal = info->duration_total;

    info->p->SetLoopMode((playForever || info->is_looped) ? (MIDIPlayer::loop_mode_enable | MIDIPlayer::loop_mode_force) : 0);

    if ( info->p->Play((float *)bytes, frames) < frames ) {
        return -1;
    }
    if ( !playForever && _info->readpos + seconds > localduration ) {
        if ( info->duration_fade ) {
            float f_fadeStart = (localduration > _info->readpos) ? localduration : _info->readpos;
            float f_fadeEnd = (_info->readpos + seconds > localdurationtotal) ? localdurationtotal : (_info->readpos + seconds);
            long framesRead = (long)(_info->readpos * (float)_info->fmt.samplerate);
            long fadeStart = f_fadeStart * (float)_info->fmt.samplerate;
            long fadeEnd = f_fadeEnd * (float)_info->fmt.samplerate;
            long fadePos;
            
            float * buff = ( float * ) bytes;
            
            float fadeScale = (info->duration_fade - (f_fadeStart - localduration)) / info->duration_fade;
            float fadeStep = 1.0f / (info->duration_fade * (float)_info->fmt.samplerate);
            for (fadePos = fadeStart; fadePos < fadeEnd; ++fadePos) {
                buff[ 0 ] *= fadeScale;
                buff[ 1 ] *= fadeScale;
                buff += 2;
                fadeScale -= fadeStep;
                if (fadeScale < 0) {
                    fadeScale = 0;
                    fadeStep = 0;
                }
            }
            
            frames = (int)(fadeEnd - framesRead);
        }
        else {
            long framesRead = (long)(_info->readpos * (float)_info->fmt.samplerate);
            long totalFrames = (long)(info->duration_total * (float)_info->fmt.samplerate);
            frames = (int)(totalFrames - framesRead);
        }
    }
    _info->readpos += (float)frames / (float)_info->fmt.samplerate;
    return frames * sizeof(float) * 2;
}

int
midi_seek_sample (DB_fileinfo_t *_info, int sample) {
    midi_info_t *info = (midi_info_t *)_info;
    info->p->Seek(sample);
    _info->readpos = sample/(float)_info->fmt.samplerate;
    return 0;
}

int
midi_seek (DB_fileinfo_t *_info, float time) {
    return midi_seek_sample (_info, time * _info->fmt.samplerate);
}

DB_playItem_t *
midi_insert (ddb_playlist_t *plt, DB_playItem_t *after, const char *fname) {
    DB_playItem_t *it = NULL;

    midi_container m;
    if (midi_open_file(&m, fname) < 0) {
        trace ("midi: failed to open %s\n", fname);
        return NULL;
    }

    unsigned long end = m.get_timestamp_end(0, true);
    end = (end * 441) / 10;
    
    it = deadbeef->pl_item_alloc_init (fname, midi_plugin.plugin.id);
    deadbeef->pl_add_meta (it, "title", NULL);
    deadbeef->plt_set_item_duration (plt, it, end / 44100.f);
    deadbeef->pl_add_meta (it, ":FILETYPE", "MID");
    after = deadbeef->plt_insert_item (plt, after, it);
    deadbeef->pl_item_unref (it);
    return after;
}

int
midi_start (void) {
    conf_driver = deadbeef->conf_get_int ("midi.driver", 0);
    conf_patch = deadbeef->conf_get_int ("midi.patch", 3);
    conf_fadeout = deadbeef->conf_get_int ("midi.fadeout", 10);
    conf_loopcount = deadbeef->conf_get_int ("midi.loopcount", 2);
    conf_play_forever = deadbeef->conf_get_int ("playback.loop", PLAYBACK_MODE_LOOP_ALL) == PLAYBACK_MODE_LOOP_SINGLE;
    return 0;
}

int
midi_stop (void) {
    return 0;
}

int
midi_message (uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2) {
    switch (id) {
        case DB_EV_CONFIGCHANGED:
            midi_start();
            break;
    }
    return 0;
}

int
midi_open_file(midi_container *out, const char *uri) {
    DB_FILE *f = deadbeef->fopen(uri);
    
    if (!f) {
        return -1;
    }
    
    const char *ext = strrchr(uri, '.');
    if (ext) ++ext;
    else ext = "";
    
    int64_t size = deadbeef->fgetlength(f);
    
    std::vector<uint8_t> data;
    
    data.resize(size);
    
    if (deadbeef->fread(&data[0], 1, size, f) < size) {
        deadbeef->fclose(f);
        return -1;
    }
    
    deadbeef->fclose(f);
    
    if (!midi_processor::process_file(data, ext, *out)) {
        return -1;
    }
    
    return 0;
}

int
midi_open_file_for_decode(midi_info_t *info) {
    info->m = new midi_container;
    if (!info->m) {
        return -1;
    }
    
    if (midi_open_file(info->m, info->path) < 0) {
        return -1;
    }

    info->m->scan_for_loops(true, true, true, true);
    
    switch ( conf_driver ) {
        case 0:
        {
            MSPlayer * msplayer = new MSPlayer;
            info->p = msplayer;
            
            msplayer->set_synth( conf_patch == 0 ? 1 : 0 );
            msplayer->set_bank( conf_patch > 0 ? conf_patch - 1 : 0 );
            msplayer->set_extp(1);
            msplayer->setSampleRate(44100);
        }
            break;

	case 1:
	{
	    struct stat buffer;
	    char midi_soundfont[1000];
	    deadbeef->conf_get_str ("midi.soundfont", "", midi_soundfont, sizeof (midi_soundfont));

            if (!*midi_soundfont || stat(midi_soundfont, &buffer) != 0)
                return -1;

	    BMPlayer * bmplayer = new BMPlayer;
	    info->p = bmplayer;

	    bmplayer->setSoundFont(midi_soundfont);
	    bmplayer->setSampleRate(44100);
	    bmplayer->setInterpolation(1);
	}
	break;
            
        default:
            return -1;
    }
    
    unsigned long framesLength = info->m->get_timestamp_end(0, true);
    unsigned long framesFade;
    unsigned long loopStart = info->m->get_timestamp_loop_start(0, true);
    unsigned long loopEnd = info->m->get_timestamp_end(0, true);

    int loopFound = 0;
    if ( loopStart != ~0UL || loopEnd != ~0UL )
        loopFound = 1;
    
    if ( loopStart == ~0UL ) loopStart = 0;
    if ( loopEnd == ~0UL ) loopEnd = framesLength;
    
    if ( loopFound ) {
        framesLength = loopStart + ( loopEnd - loopStart ) * conf_loopcount;
        framesFade = conf_fadeout * 1000;
        info->is_looped = 1;
    }
    else {
        framesLength += 1000;
        framesFade = 0;
        info->is_looped = 0;
    }
    
    info->duration = framesLength / 1000.0f;
    info->duration_fade = framesFade / 1000.0f;
    info->duration_total = info->duration + info->duration_fade;
    
    unsigned int loop_mode = framesFade ? MIDIPlayer::loop_mode_enable | MIDIPlayer::loop_mode_force : 0;
    unsigned int clean_flags = midi_container::clean_flag_emidi;
    
    if ( !info->p->Load( *info->m, 0, loop_mode, clean_flags) ) {
        return -1;
    }

    return 0;
}



extern "C"
__attribute__ ((visibility ("default")))
DB_plugin_t *
midi_load (DB_functions_t *api) {
    deadbeef = api;
    return DB_PLUGIN (&midi_plugin);
}

static const char *exts[] = { "mid","midi","kar","rmi","mids","mds","hmi","hmp","mus","xmi","lds",NULL };

static const char settings_dlg[] =
  "property \"MIDI Driver to use\" entry midi.driver 0;\n"
  "property \"MIDI SoundFont to use with driver 1\" entry midi.soundfont \"\";\n"
  "property \"MIDI patch set to use with specific driver\" entry midi.patch 3;\n"
  "property \"Fadeout length (seconds)\" entry midi.fadeout 10;\n"
  "property \"Play loops nr. of times (if available)\" entry midi.loopcount 2;\n"
;
// define plugin interface
DB_decoder_t midi_plugin = {
    .plugin = {
            .type = DB_PLUGIN_DECODER,
	    .api_vmajor = 1,
	    .api_vminor = 0,
            .version_major = 1,
            .version_minor = 0,
            .id = "midi",
            .name = "Midi player",
            .descr = "MIDI player based on multiple synthesizers\n\nDefaults to Doom 2 MIDI synthesizer.",
            .copyright = 
        "Midi plugin for DeaDBeeF Player\n"
        "Copyright (C) 2009-2014 Alexey Yakovenko\n"
        "Copyright (C) 2016-2019 Christopher Snowhill\n"
        "\n"
        "This software is provided 'as-is', without any express or implied\n"
        "warranty.  In no event will the authors be held liable for any damages\n"
        "arising from the use of this software.\n"
        "\n"
        "Permission is granted to anyone to use this software for any purpose,\n"
        "including commercial applications, and to alter it and redistribute it\n"
        "freely, subject to the following restrictions:\n"
        "\n"
        "1. The origin of this software must not be misrepresented; you must not\n"
        " claim that you wrote the original software. If you use this software\n"
        " in a product, an acknowledgment in the product documentation would be\n"
        " appreciated but is not required.\n"
        "\n"
        "2. Altered source versions must be plainly marked as such, and must not be\n"
        " misrepresented as being the original software.\n"
        "\n"
        "3. This notice may not be removed or altered from any source distribution.\n"
    ,
            .website = "http://gitlab.kode54.net/kode54/deadbeef_plugins",
            .start = midi_start,
            .stop = midi_stop,
            .message = midi_message,
            .configdialog = settings_dlg,
	},
    .open = midi_open,
    .init = midi_init,
    .free = midi_free,
    .read = midi_read,
    .seek = midi_seek,
    .seek_sample = midi_seek_sample,
    .insert = midi_insert,
    .exts = exts,
};
