/*
    libopenmpt plugin for DeaDBeeF Player
    Copyright (C) 2019-2024 Christopher Snowhill
 
    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/

#include <stdlib.h>
#include <string.h>
#include <deadbeef/deadbeef.h>
#include <libopenmpt/libopenmpt.h>

#define trace(...) { fprintf(stderr, __VA_ARGS__); }

static const char *exts_placeholder[]={NULL};

static DB_decoder_t plugin;
static DB_functions_t *deadbeef;

static int conf_play_forever = 0;

/*
 * libopenmpt decoder plugin for Deadbeef
 */

# define strdup(s)							      \
  (__extension__							      \
    ({									      \
      const char *__old = (s);						      \
      size_t __len = strlen (__old) + 1;				      \
      char *__new = (char *) malloc (__len);			      \
      (char *) memcpy (__new, __old, __len);				      \
    }))

typedef struct {
	DB_fileinfo_t info;
	openmpt_module *mod;
	int subsong;
	int can_loop;
	int position;
	int fadesamples;
	int totalsamples;
} mod_info_t;

static int samplerate = 44100;
static int channels = 2;
static double looptimes = 2.0;
static double fadeseconds = 10.0;
static double fadedelayseconds = 10.0;

#define COPYRIGHT_STR \
	"- deadbeef-libopenmpt -\n" \
	"Copyright (c) 2019 Christopher Snowhill <kode54@gmail.com>\n" \
	"\n" \
	"Licensed under the same terms as libopenmpt (see below.)\n" \
	"\n" \
	"- libopenmpt -\n" \
	"Copyright (c) 2004-2019, OpenMPT contributors\n" \
	"Copyright (c) 1997-2003, Olivier Lapicque\n" \
	"All rights reserved.\n" \
	"\n" \
	"Redistribution and use in source and binary forms, with or without\n" \
	"modification, are permitted provided that the following conditions are met:\n" \
	"    * Redistributions of source code must retain the above copyright\n" \
	"      notice, this list of conditions and the following disclaimer.\n" \
	"    * Redistributions in binary form must reproduce the above copyright\n" \
	"      notice, this list of conditions and the following disclaimer in the\n" \
	"      documentation and/or other materials provided with the distribution.\n" \
	"    * Neither the name of the OpenMPT project nor the\n" \
	"      names of its contributors may be used to endorse or promote products\n" \
	"      derived from this software without specific prior written permission.\n" \
	"\n" \
	"THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY\n" \
	"EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED\n" \
	"WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\n" \
	"DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY\n" \
	"DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES\n" \
	"(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;\n" \
	"LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND\n" \
	"ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n" \
	"(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n" \
	"SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

static DB_fileinfo_t *mod_open(uint32_t hints) {
	DB_fileinfo_t *_info = malloc(sizeof(mod_info_t));
	mod_info_t *info = (mod_info_t *)_info;
	memset(info, 0, sizeof (mod_info_t));
	info->can_loop = hints & DDB_DECODER_HINT_CAN_LOOP;
	return _info;
}

static int mod_init(DB_fileinfo_t *_info, DB_playItem_t *it) {
	mod_info_t *info = (mod_info_t *)_info;

	deadbeef->pl_lock ();
	const char *fname = strdup(deadbeef->pl_find_meta (it, ":URI"));
	int i = deadbeef->pl_find_meta_int(it, ":TRACKNUM", 0);
	deadbeef->pl_unlock ();

	DB_FILE *file = deadbeef->fopen(fname);
	if (!file) return -1;

	off_t size = deadbeef->fgetlength(file);

	void * filedata = malloc(size);
	if (!file) {
		deadbeef->fclose(file);
		return -1;
	}

	deadbeef->fread(filedata, 1, size, file);
	deadbeef->fclose(file);

	info->mod = openmpt_module_create_from_memory2(filedata, size, openmpt_log_func_silent, 0, 0, 0, 0, 0 ,0);
	free(filedata);

	if (!info->mod) return -1;

	openmpt_module_select_subsong(info->mod, i);

	openmpt_module_ctl_set_text(info->mod, "play.at_end", "continue");

	info->fadesamples = fadeseconds * samplerate;
	info->totalsamples = openmpt_module_get_duration_seconds(info->mod) * (double)samplerate;
	_info->fmt.is_float = 1;
	_info->fmt.bps = 32;
	_info->fmt.channels = channels;
	_info->fmt.samplerate  = samplerate;
	for (i = 0; i < _info->fmt.channels; i++)
		_info->fmt.channelmask |= 1 << i;
	_info->readpos = 0;
	_info->plugin = &plugin;
	return 0;
}

static void mod_free(DB_fileinfo_t *_info) {
	mod_info_t *info = (mod_info_t *)_info;
	openmpt_module_destroy(info->mod);
	if (info) free(info);
}

static int mod_read(DB_fileinfo_t *_info, char *bytes, int size) {
	int i, j;
	mod_info_t *info = (mod_info_t *)_info;
	int32_t sample_count = size / (_info->fmt.channels * sizeof(float)),
	        fade_start = info->totalsamples - info->fadesamples,
                fade_end = info->totalsamples,
                fade_samples = info->fadesamples;
    
	int playForever = info->can_loop && conf_play_forever;

	/* Past the end? Let deadbeef know. */
	if (!playForever && info->position >= info->totalsamples) return 0;

	/* Report true position */
	_info->readpos = openmpt_module_get_position_seconds(info->mod);

	/* Do the actual rendering. */
	int rval;
	do {
		rval = openmpt_module_read_interleaved_float_stereo(info->mod, _info->fmt.samplerate, sample_count, (float *)bytes);
		
		if (!playForever && !rval) return 0;
	} while (!rval);

	/* If we are overlapping any piece after the fade starts, we must fade out here.
         * TODO: Desperately needs cleanup. It's _AWFUL_.
         */
	if (!playForever && (info->position > fade_start || info->position + sample_count > fade_start)) {
		float *buf = (float *)bytes;
		for (i = 0; i < sample_count; ++i) {
			int pos = i + info->position;
			if (pos > fade_start) {
				int samples_into_fade = pos - fade_start;
				double fadedness = (double)(fade_samples-samples_into_fade)/fade_samples;
				for (j = 0; j < _info->fmt.channels; j++)
					buf[i * _info->fmt.channels + j] = buf[i * _info->fmt.channels + j] * fadedness;
			} else if (pos > fade_end) {
				info->position = info->totalsamples;
				return i * sizeof(float) * _info->fmt.channels;
			}
		}
	}
	info->position += sample_count;
	return size;
}

static int mod_seek(DB_fileinfo_t *_info, float time) {
	mod_info_t *info = (mod_info_t *)_info;
	time = openmpt_module_set_position_seconds(info->mod, time);
	info->position = time * _info->fmt.samplerate;
	return 0;
}

static int mod_seek_sample(DB_fileinfo_t *_info, int sample) {
	return mod_seek(_info, (double)sample / _info->fmt.samplerate);
}

static DB_playItem_t *mod_insert(ddb_playlist_t *plt, DB_playItem_t *after, const char *fname) {
	char temp[160];
	DB_FILE *file = deadbeef->fopen(fname);
	if (!file) return after;

	off_t size = deadbeef->fgetlength(file);

	void *filedata = malloc(size);
	if (!filedata) {
		deadbeef->fclose(file);
		return after;
	}

	deadbeef->fread(filedata, 1, size, file);
	deadbeef->fclose(file);

	openmpt_module * mod = openmpt_module_create_from_memory2(filedata, size, openmpt_log_func_silent, 0, 0, 0, 0, 0, 0);
	free(filedata);
	if (!mod) return after;

	int subsong_count = openmpt_module_get_num_subsongs(mod);

	for (int i = 0; i < subsong_count; ++i) {
		openmpt_module_select_subsong(mod, i);
		double length = openmpt_module_get_duration_seconds(mod);

		DB_playItem_t *it = deadbeef->pl_item_alloc_init(fname, plugin.plugin.id);

		deadbeef->pl_replace_meta(it, ":FILETYPE", "mod");
		deadbeef->plt_set_item_duration(plt, it, length);

		deadbeef->pl_set_meta_int (it, ":TRACKNUM", i);

		const char * title = openmpt_module_get_subsong_name(mod, i);

		if (title)
			deadbeef->pl_add_meta(it, "title", title);

		snprintf(temp, sizeof(temp), "%d", samplerate);
		deadbeef->pl_add_meta(it, ":SAMPLERATE", temp);
		snprintf(temp, sizeof(temp), "%d", channels);
		deadbeef->pl_add_meta(it, ":CHANNELS", temp);
		deadbeef->pl_add_meta(it, ":BPS", "32");

		after = deadbeef->plt_insert_item(plt, after, it);
		deadbeef->pl_item_unref (it);
	}

	openmpt_module_destroy(mod);

	return after;
}

static int mod_start(void) {
	const char * ext_list;
	size_t ext_list_size;
	size_t i;

	conf_play_forever = deadbeef->conf_get_int ("playback.loop", PLAYBACK_MODE_LOOP_ALL) == PLAYBACK_MODE_LOOP_SINGLE;

	ext_list = openmpt_get_supported_extensions();

	for (ext_list_size = 0, i = 0; ext_list[i];) {
		const char * ext = ext_list + i;
		const char * ext_end = strchr(ext, ';');
		if (!ext_end) ext_end = ext + strlen(ext);
		else ++ext_end;
		++ext_list_size;
		i = ext_end - ext_list;
	}

	plugin.exts = (const char **) calloc(sizeof(*plugin.exts), ext_list_size + 1);

	for (i = 0; i < ext_list_size; ++i) {
		const char * ext = ext_list;
		const char * ext_end = strchr(ext, ';');
		if (!ext_end) ext_end = ext + strlen(ext);
		plugin.exts[i] = strndup(ext, ext_end - ext);
		if (*ext_end) ext_list = ext_end + 1;
	}

	plugin.exts[i] = NULL;

	return 0;
}

static int mod_stop(void) {
	const char ** exts = plugin.exts;
	while (*exts) {
		free((void*)(*exts));
		++exts;
	}
	free(plugin.exts); plugin.exts = exts_placeholder;
	return 0;
}

int
mod_message (uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2) {
    switch (id) {
        case DB_EV_CONFIGCHANGED:
            conf_play_forever = deadbeef->conf_get_int ("playback.loop", PLAYBACK_MODE_LOOP_ALL) == PLAYBACK_MODE_LOOP_SINGLE;
            break;
    }
    return 0;
}


// define plugin interface
static DB_decoder_t plugin = {
	DB_PLUGIN_SET_API_VERSION
	.plugin.version_major = 0,
	.plugin.version_minor = 1,
	.plugin.type = DB_PLUGIN_DECODER,
	.plugin.name = "libopenmpt",
	.plugin.id = "mod",
	.plugin.descr = "Decodes a variety of module music formats.",
	.plugin.copyright = COPYRIGHT_STR,
	.plugin.start = mod_start,
	.plugin.stop = mod_stop,
	.open = mod_open,
	.init = mod_init,
	.free = mod_free,
	.read = mod_read,
	.seek = mod_seek,
	.seek_sample = mod_seek_sample,
	.insert = mod_insert,
	.exts = exts_placeholder,
    .plugin.message = mod_message,
};

__attribute__ ((visibility ("default")))
DB_plugin_t *openmpt_load(DB_functions_t *api) {
	deadbeef = api;
	return DB_PLUGIN (&plugin);
}
