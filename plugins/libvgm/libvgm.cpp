/*
    libvgm plugin for DeaDBeeF Player
    Copyright (C) 2024 Christopher Snowhill
 
    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.

    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.
*/

#include <stdlib.h>
#include <string.h>
#include <deadbeef/deadbeef.h>

#include <cmath>

#include <libvgm/player/playera.hpp>
#include <libvgm/player/playerbase.hpp>
#include <libvgm/utils/DataLoader.h>

#include <libvgm/emu/EmuCores.h>
#include <libvgm/emu/Resampler.h>
#include <libvgm/emu/SoundDevs.h>
#include <libvgm/player/droplayer.hpp>
#include <libvgm/player/gymplayer.hpp>
#include <libvgm/player/s98player.hpp>
#include <libvgm/player/vgmplayer.hpp>
#include <libvgm/utils/FileLoader.h>
#include <libvgm/utils/MemoryLoader.h>

#define trace(...) { fprintf(stderr, __VA_ARGS__); }

extern DB_decoder_t libvgm_plugin;
static DB_functions_t *deadbeef;

static int conf_play_forever = 0;

#ifdef _DEBUG
const int logLevel = DEVLOG_DEBUG;
#else
const int logLevel = DEVLOG_INFO;
#endif

static std::string FCC2Str(UINT32 fcc) {
	std::string result(4, '\0');
	result[0] = (char)((fcc >> 24) & 0xFF);
	result[1] = (char)((fcc >> 16) & 0xFF);
	result[2] = (char)((fcc >> 8) & 0xFF);
	result[3] = (char)((fcc >> 0) & 0xFF);
	return result;
}

/*
 * libvgm decoder plugin for Deadbeef
 */

typedef struct {
	DB_fileinfo_t info;
	UINT8* fileData;
	DATA_LOADER* dLoad;
	PlayerA* mainPlr;
	int can_loop;
	double sampleRate;
	long loopCount;
	double fadeTime;
	long length;
	int trackEnded;
} libvgm_info_t;

static UINT8 FilePlayCallback(PlayerBase* player, void* userParam, UINT8 evtType, void* evtParam) {
	libvgm_info_t* info = (libvgm_info_t*)(userParam);
	switch(evtType) {
		case PLREVT_START:
			// printf("Playback started.\n");
			break;
		case PLREVT_STOP:
			// printf("Playback stopped.\n");
			break;
		case PLREVT_LOOP: {
			// UINT32* curLoop = (UINT32*)evtParam;
			// if (player->GetState() & PLAYSTATE_SEEK)
			//	break;
		} break;
		case PLREVT_END:
			if(info) {
				if(info->trackEnded)
					break;
				info->trackEnded = 1;
			}
			break;
	}
	return 0x00;
}

#include "yrw801.h"

static DATA_LOADER* RequestFileCallback(void* userParam, PlayerBase* player, const char* fileName) {
	DATA_LOADER* dLoad;
	if(strcmp(fileName, "yrw801.rom") == 0) {
		dLoad = MemoryLoader_Init(yrw801_rom, sizeof(yrw801_rom));
	} else {
		dLoad = FileLoader_Init(fileName);
	}
	UINT8 retVal = DataLoader_Load(dLoad);
	if(!retVal)
		return dLoad;
	DataLoader_Deinit(dLoad);
	return NULL;
}

static const char* LogLevel2Str(UINT8 level) {
	static const char* LVL_NAMES[6] = { " ??? ", "Error", "Warn ", "Info ", "Debug", "Trace" };
	if(level >= (sizeof(LVL_NAMES) / sizeof(LVL_NAMES[0])))
		level = 0;
	return LVL_NAMES[level];
}

static void PlayerLogCallback(void* userParam, PlayerBase* player, UINT8 level, UINT8 srcType,
                              const char* srcTag, const char* message) {
	if(level > logLevel)
		return; // don't print messages with higher verbosity than current log level
	if(srcType == PLRLOGSRC_PLR) {
		trace("[%s] %s: %s", LogLevel2Str(level), player->GetPlayerName(), message);
	} else {
		trace("[%s] %s %s: %s", LogLevel2Str(level), player->GetPlayerName(), srcTag, message);
	}
	return;
}

const int numChannels = 2;
const int numBitsPerSample = 24;
const int smplAlloc = 2048;
const int masterVol = 0x10000; // Fixed point 16.16

#define COPYRIGHT_STR \
	"- deadbeef-libvgm -\n" \
	"Copyright (c) 2024 Christopher Snowhill <kode54@gmail.com>\n" \
	"\n" \
	"Licensed under the 3 clause BSD License. (See below)\n" \
	"\n" \
	"- libvgm -\n" \
	"Copyright (c) 2015-2024 Valley Bell and libvgm contributors\n" \
	"All rights reserved.\n" \
	"\n" \
	"Redistribution and use in source and binary forms, with or without\n" \
	"modification, are permitted provided that the following conditions are met:\n" \
	"    * Redistributions of source code must retain the above copyright\n" \
	"      notice, this list of conditions and the following disclaimer.\n" \
	"    * Redistributions in binary form must reproduce the above copyright\n" \
	"      notice, this list of conditions and the following disclaimer in the\n" \
	"      documentation and/or other materials provided with the distribution.\n" \
	"    * Neither the name of the OpenMPT project nor the\n" \
	"      names of its contributors may be used to endorse or promote products\n" \
	"      derived from this software without specific prior written permission.\n" \
	"\n" \
	"THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY\n" \
	"EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED\n" \
	"WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\n" \
	"DISCLAIMED. IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY\n" \
	"DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES\n" \
	"(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;\n" \
	"LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND\n" \
	"ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n" \
	"(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n" \
	"SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."

static int PlayerInit(libvgm_info_t *info, const char *uri) {
	info->sampleRate = 44100.0;

	info->loopCount = 2;

	info->fadeTime = 5.0;

	uint32_t maxLoops = conf_play_forever ? 0 : (uint32_t)info->loopCount;

	info->mainPlr = new PlayerA;
	info->mainPlr->RegisterPlayerEngine(new VGMPlayer);
	info->mainPlr->RegisterPlayerEngine(new S98Player);
	info->mainPlr->RegisterPlayerEngine(new DROPlayer);
	info->mainPlr->RegisterPlayerEngine(new GYMPlayer);
	info->mainPlr->SetEventCallback(FilePlayCallback, (void*)(info));
	info->mainPlr->SetFileReqCallback(RequestFileCallback, NULL);
	info->mainPlr->SetLogCallback(PlayerLogCallback, NULL);
	{
		PlayerA::Config pCfg = info->mainPlr->GetConfiguration();
		pCfg.masterVol = masterVol;
		pCfg.loopCount = maxLoops;
		pCfg.fadeSmpls = (int)std::ceil(info->sampleRate * info->fadeTime); // fade over configured duration
		pCfg.endSilenceSmpls = info->sampleRate / 2; // 0.5 seconds of silence at the end
		pCfg.pbSpeed = 1.0;
		info->mainPlr->SetConfiguration(pCfg);
	}
	info->mainPlr->SetOutputSettings((int)std::ceil(info->sampleRate), numChannels, numBitsPerSample, smplAlloc);

	DB_FILE *file = deadbeef->fopen(uri);
	if(!file)
		return -1;

	size_t size = deadbeef->fgetlength(file);

	info->fileData = (UINT8*)malloc(size);
	if(!info->fileData) {
		deadbeef->fclose(file);
		return -1;
	}

	size_t bytesRead = deadbeef->fread(info->fileData, 1, size, file);
	deadbeef->fclose(file);

	if(bytesRead != size)
		return -1;

	info->dLoad = MemoryLoader_Init(info->fileData, (unsigned int)size);
	if(!info->dLoad)
		return -1;

	DataLoader_SetPreloadBytes(info->dLoad, 0x100);
	if(DataLoader_Load(info->dLoad))
		return -1;

	if(info->mainPlr->LoadFile(info->dLoad))
		return -1;

	PlayerBase* player = info->mainPlr->GetPlayer();

	info->mainPlr->SetLoopCount(maxLoops);
	if(player->GetPlayerType() == FCC_VGM) {
		VGMPlayer* vgmplay = dynamic_cast<VGMPlayer*>(player);
		info->mainPlr->SetLoopCount(vgmplay->GetModifiedLoopCount(maxLoops));
	}

	info->length = player->Tick2Second(player->GetTotalTicks()) * info->sampleRate;

	return 0;
}

static DB_fileinfo_t *libvgm_open(uint32_t hints) {
	DB_fileinfo_t *_info = (DB_fileinfo_t *) malloc(sizeof(libvgm_info_t));
	libvgm_info_t *info = (libvgm_info_t *)_info;
	memset(info, 0, sizeof (libvgm_info_t));
	info->can_loop = hints & DDB_DECODER_HINT_CAN_LOOP;
	return _info;
}

static void libvgm_free(DB_fileinfo_t *_info) {
	libvgm_info_t *info = (libvgm_info_t *)_info;
	if(info) {
		if(info->mainPlr) {
			info->mainPlr->Stop();
			info->mainPlr->UnloadFile();

			delete info->mainPlr;
		}
		if(info->dLoad) {
			DataLoader_Deinit(info->dLoad);
		}
		if(info->fileData) {
			free(info->fileData);
		}
		free(info);
	}
}

static DB_fileinfo_t *
libvgm_open2(uint32_t hints, DB_playItem_t *it) {
	libvgm_info_t *info = (libvgm_info_t *) libvgm_open(hints);
	if (!info) {
		return NULL;
	}

	deadbeef->pl_lock();
		const char *uri = strdupa(deadbeef->pl_find_meta (it, ":URI"));
	deadbeef->pl_unlock();

	if (PlayerInit(info, uri)) {
		libvgm_free((DB_fileinfo_t *)info);
		return NULL;
	}

	return (DB_fileinfo_t *) info;
}

static int libvgm_init(DB_fileinfo_t *_info, DB_playItem_t *it) {
	libvgm_info_t *info = (libvgm_info_t *)_info;

	if (!info->mainPlr) {
		deadbeef->pl_lock ();
			const char *uri = strdupa(deadbeef->pl_find_meta (it, ":URI"));
		deadbeef->pl_unlock ();

		if (PlayerInit(info, uri)) {
			return -1;
		}
	}

	info->trackEnded = 0;

	info->mainPlr->Start();

	_info->fmt.is_float = 0;
	_info->fmt.bps = numBitsPerSample;
	_info->fmt.channels = numChannels;
	_info->fmt.samplerate  = info->sampleRate;
	for (int i = 0; i < _info->fmt.channels; i++)
		_info->fmt.channelmask |= 1 << i;
	_info->readpos = 0;
	_info->plugin = &libvgm_plugin;
	return 0;
}

static int libvgm_read(DB_fileinfo_t *_info, char *bytes, int size) {
	libvgm_info_t *info = (libvgm_info_t *)_info;
	if (info->trackEnded)
		return 0;

	uint32_t frames = size / (numChannels * (numBitsPerSample / 8));

	int playForever = info->can_loop && conf_play_forever;

	uint32_t maxLoops = playForever ? 0 : (uint32_t)info->loopCount;

	PlayerBase* player = info->mainPlr->GetPlayer();
	info->mainPlr->SetLoopCount(maxLoops);
	if(player->GetPlayerType() == FCC_VGM) {
		VGMPlayer* vgmplay = dynamic_cast<VGMPlayer*>(player);
		info->mainPlr->SetLoopCount(vgmplay->GetModifiedLoopCount(maxLoops));
	}

	uint32_t framesDone = 0;

	while(framesDone < frames) {
		uint32_t framesToDo = frames - framesDone;
		if(framesToDo > smplAlloc)
			framesToDo = smplAlloc;

		int numSamples = framesToDo * numChannels * (numBitsPerSample / 8);

		info->mainPlr->Render(numSamples, bytes);

		bytes += numSamples;
		framesDone += framesToDo;
	}

	/* Report true position */
	_info->readpos = info->mainPlr->GetCurTime(PLAYTIME_LOOP_EXCL | PLAYTIME_TIME_FILE);

	return size;
}

static int libvgm_seek_sample(DB_fileinfo_t *_info, int sample) {
	libvgm_info_t *info = (libvgm_info_t *)_info;

	info->mainPlr->Seek(sample, PLAYPOS_SAMPLE);

	return 0;
}

static int libvgm_seek(DB_fileinfo_t *_info, float time) {
	libvgm_info_t *info = (libvgm_info_t *)_info;

	int sample = (int)std::floor(time * info->sampleRate);

	return libvgm_seek_sample(_info, sample);
}

static DB_playItem_t *libvgm_insert(ddb_playlist_t *plt, DB_playItem_t *after, const char *fname) {
	DB_FILE *file = deadbeef->fopen(fname);
	if (!file) return after;

	off_t size = deadbeef->fgetlength(file);

	void *fileData = malloc(size);
	if (!fileData) {
		deadbeef->fclose(file);
		return after;
	}

	deadbeef->fread(fileData, 1, size, file);
	deadbeef->fclose(file);

	PlayerA mainPlr;
	mainPlr.RegisterPlayerEngine(new VGMPlayer);
	mainPlr.RegisterPlayerEngine(new S98Player);
	mainPlr.RegisterPlayerEngine(new DROPlayer);
	mainPlr.RegisterPlayerEngine(new GYMPlayer);
	mainPlr.SetEventCallback(FilePlayCallback, NULL);
	mainPlr.SetFileReqCallback(RequestFileCallback, NULL);
	mainPlr.SetLogCallback(PlayerLogCallback, NULL);
	mainPlr.SetOutputSettings(44100, 2, 24, 2048);

	DATA_LOADER* dLoad = MemoryLoader_Init((const UINT8 *)fileData, (unsigned int)size);
	if(!dLoad) {
		free(fileData);
		return 0;
	}

	DataLoader_SetPreloadBytes(dLoad, 0x100);
	if(DataLoader_Load(dLoad)) {
		DataLoader_Deinit(dLoad);
		free(fileData);
		return 0;
	}

	if(mainPlr.LoadFile(dLoad)) {
		DataLoader_Deinit(dLoad);
		free(fileData);
		return 0;
	}

	const char *title  = NULL;
	const char *artist = NULL;
	const char *album  = NULL;
	const char *date   = NULL;

	PlayerBase* player = mainPlr.GetPlayer();

	double lengthSeconds = player->Tick2Second(player->GetTotalTicks());

	const char* const* tagList = player->GetTags();
	for(const char* const* t = tagList; *t; t += 2) {
		if(!strcmp(t[0], "TITLE"))
			title = t[1];
		else if(!strcmp(t[0], "ARTIST"))
			artist = t[1];
		else if(!strcmp(t[0], "GAME"))
			album = t[1];
		else if(!strcmp(t[0], "DATE"))
			date = t[1];
	}

	DB_playItem_t *it = deadbeef->pl_item_alloc_init(fname, libvgm_plugin.plugin.id);

	deadbeef->pl_replace_meta(it, ":FILETYPE", "vgm");
	deadbeef->plt_set_item_duration(plt, it, lengthSeconds);

	if (title)
		deadbeef->pl_add_meta(it, "title", title);
	if (artist)
		deadbeef->pl_add_meta(it, "artist", artist);
	if (album)
		deadbeef->pl_add_meta(it, "album", album);
	if (date)
		deadbeef->pl_add_meta(it, "date", date);

	char temp[120];

	snprintf(temp, sizeof(temp), "%d", 44100);
	deadbeef->pl_add_meta(it, ":SAMPLERATE", temp);
	snprintf(temp, sizeof(temp), "%d", numChannels);
	deadbeef->pl_add_meta(it, ":CHANNELS", temp);
	deadbeef->pl_add_meta(it, ":BPS", "24");

	PLR_SONG_INFO sInf;
	player->GetSongInfo(sInf);

	snprintf(temp, 119, "%s v%X.%02X", FCC2Str(sInf.format).c_str(), sInf.fileVerMaj, sInf.fileVerMin);
	temp[119] = '\0';

	deadbeef->pl_add_meta(it, ":CODEC", temp);

	mainPlr.UnloadFile();
	DataLoader_Deinit(dLoad);
	free(fileData);

	after = deadbeef->plt_insert_item(plt, after, it);
	deadbeef->pl_item_unref (it);

	return after;
}

static int libvgm_start(void) {
	conf_play_forever = deadbeef->conf_get_int ("playback.loop", PLAYBACK_MODE_LOOP_ALL) == PLAYBACK_MODE_LOOP_SINGLE;

	return 0;
}

static int libvgm_stop(void) {
	return 0;
}

int
libvgm_message (uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2) {
    switch (id) {
        case DB_EV_CONFIGCHANGED:
            conf_play_forever = deadbeef->conf_get_int ("playback.loop", PLAYBACK_MODE_LOOP_ALL) == PLAYBACK_MODE_LOOP_SINGLE;
            break;
    }
    return 0;
}

static const char *exts[] = { "vgm", "vgz", "s98", "dro", "gym", NULL };

// define plugin interface
DB_decoder_t libvgm_plugin = {
	.plugin = {
		.type = DB_PLUGIN_DECODER,
		.api_vmajor = 1,
		.api_vminor = 0,
		.version_major = 1,
		.version_minor = 0,
		.id = "libvgm",
		.name = "libvgm chip log player",
		.descr = "Decodes a variety of chip register log music formats.",
		.copyright = COPYRIGHT_STR,
		.website = "https://bitbucket.org/losnoco/deadbeef_plugins",
		.start = libvgm_start,
		.stop = libvgm_stop,
    	.message = libvgm_message,
	},
	.open = libvgm_open,
	.init = libvgm_init,
	.free = libvgm_free,
	.read = libvgm_read,
	.seek = libvgm_seek,
	.seek_sample = libvgm_seek_sample,
	.insert = libvgm_insert,
	.exts = exts,
	.open2 = libvgm_open2,
};

__attribute__ ((visibility ("default")))
DB_plugin_t *libvgm_load(DB_functions_t *api) {
	deadbeef = api;
	return DB_PLUGIN (&libvgm_plugin);
}
