/*
    vgmstream plugin for DeaDBeeF
    Copyright (c) 2014 John Chadwick <johnwchadwick@gmail.com>
    Copyright (c) 2016-2024 Christopher Snowhill <kode54@gmail.com>

    Broken in two parts: need a SF implementation for VGMStream, and a decoder implementation for Deadbeef.
*/


#include <stdlib.h>
#include <string.h>
#include <deadbeef/deadbeef.h>
#include <vgmstream/vgmstream.h>
#include <vgmstream/api.h>

#define trace(...) { fprintf(stderr, __VA_ARGS__); }

#define PLUGIN_VERSION "r1896-86-gc003ad56"

static const char *exts_placeholder[]={NULL};

static DB_decoder_t plugin;
static DB_functions_t *deadbeef;

static int conf_play_forever = 0;

static const char* tagfile_name = "!tags.m3u";

#define MIN_BUFFER_SIZE 576

# define strdupa(s)							      \
  (__extension__							      \
    ({									      \
      const char *__old = (s);						      \
      size_t __len = strlen (__old) + 1;				      \
      char *__new = (char *) malloc (__len);				      \
      (char *) memcpy (__new, __old, __len);				      \
    }))

/*
 * VGMStream Streamfile implementation for Deadbeef 
 */

typedef struct _DBSTREAMFILE {
	STREAMFILE sf;
	DB_FILE *file;
	off_t offset;
	char name[PATH_LIMIT];
} DBSTREAMFILE;

static void dbsf_seek(DBSTREAMFILE *this, off_t offset) {
	if (deadbeef->fseek(this->file, offset, SEEK_SET) == 0)
		this->offset = offset;
	else
		this->offset = deadbeef->ftell(this->file);
}

static off_t dbsf_get_size(DBSTREAMFILE *this) {
	return deadbeef->fgetlength(this->file);
}

static off_t dbsf_get_offset(DBSTREAMFILE *this) {
	return this->offset;
}

static void dbsf_get_name(DBSTREAMFILE *this, char *buffer, size_t length) {
	strncpy(buffer, this->name, length);
	buffer[length-1]='\0';
}

static size_t dbsf_read(DBSTREAMFILE *this, uint8_t *dest, off_t offset, size_t length) {
	size_t read;
	if (this->offset != offset)
		dbsf_seek(this, offset);
	read = deadbeef->fread(dest, 1, length, this->file);
	if (read > 0)
		this->offset += read;
	return read;
}

static void dbsf_close(DBSTREAMFILE *this) {
	deadbeef->fclose(this->file);
	free(this);
}

static STREAMFILE *dbsf_create_from_path(const char *path);
static STREAMFILE *dbsf_open(DBSTREAMFILE *this, const char *const filename,size_t buffersize) {
	if (!filename) return NULL;
	return dbsf_create_from_path(filename);
}

static STREAMFILE *dbsf_create(DB_FILE *file, const char *path) {
	DBSTREAMFILE *streamfile = malloc(sizeof(DBSTREAMFILE));

	if (!streamfile) return NULL;

	memset(streamfile,0,sizeof(DBSTREAMFILE));
	streamfile->sf.read = (void*)dbsf_read;
	streamfile->sf.get_size = (void*)dbsf_get_size;
	streamfile->sf.get_offset = (void*)dbsf_get_offset;
	streamfile->sf.get_name = (void*)dbsf_get_name;
	streamfile->sf.open = (void*)dbsf_open;
	streamfile->sf.close = (void*)dbsf_close;
	streamfile->file = file;
	streamfile->offset = 0;
	strncpy(streamfile->name, path, sizeof(streamfile->name));

	return &streamfile->sf;
}

STREAMFILE *dbsf_create_from_path(const char *path) {
	DB_FILE *file = deadbeef->fopen(path);

	if (!file) return NULL;

	return dbsf_create(file, path);
}

VGMSTREAM *init_vgmstream_from_dbfile(size_t subsong, const char *path) {
	STREAMFILE *sf;
	VGMSTREAM *vgm;

	sf = dbsf_create_from_path(path);
	if (!sf) return NULL;

	sf->stream_index = subsong;

	vgm = init_vgmstream_from_STREAMFILE(sf);
	close_streamfile(sf);
	if (!vgm) return NULL;

	return vgm;
}


/*
 * VGMStream decoder plugin for Deadbeef
 */

typedef struct {
	DB_fileinfo_t info;
	VGMSTREAM *s;
	int can_loop;
	int position;
	int length_samples;
	int input_channels;
	int output_channels;
} vgm_info_t;

static int downmix_channels = 2;
static double looptimes = 2.0;
static double fadeseconds = 10.0;
static double fadedelayseconds = 10.0;

#define COPYRIGHT_STR \
	"- deadbeef-vgmstream -\n" \
	"Copyright (c) 2014 John Chadwick <johnwchadwick@gmail.com>\n" \
	"\n" \
	"Licensed under the same terms as vgmstream (see below.)\n" \
	"\n" \
	"- vgmstream -\n" \
	"Copyright (c) 2008-2010 Adam Gashlin, Fastelbja, Ronny Elfert\n" \
	"Portions Copyright (c) 2004-2008, Marko Kreen\n" \
	"Portions Copyright 2001-2007  jagarl / Kazunori Ueno <jagarl@creator.club.ne.jp>\n" \
	"Portions Copyright (c) 1998, Justin Frankel/Nullsoft Inc.\n" \
	"Portions Copyright (C) 2006 Nullsoft, Inc.\n" \
	"Portions Copyright (c) 2005-2007 Paul Hsieh\n" \
	"Portions Public Domain originating with Sun Microsystems\n" \
	"\n" \
	"Permission to use, copy, modify, and distribute this software for any\n" \
	"purpose with or without fee is hereby granted, provided that the above\n" \
	"copyright notice and this permission notice appear in all copies.\n" \
	"\n" \
	"THE SOFTWARE IS PROVIDED \"AS IS\" AND THE AUTHOR DISCLAIMS ALL WARRANTIES\n" \
	"WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF\n" \
	"MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR\n" \
	"ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES\n" \
	"WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN\n" \
	"ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF\n" \
	"OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.\n"

static vgm_info_t *
vgm_open_int(uint32_t hints) {
	vgm_info_t *info = (vgm_info_t *) calloc(1, sizeof(vgm_info_t));
	if (info && hints & DDB_DECODER_HINT_CAN_LOOP) {
		info->can_loop = 1;
	}
	return info;
}

static DB_fileinfo_t *
vgm_open(uint32_t hints) {
	return (DB_fileinfo_t *) vgm_open_int(hints);
}

static DB_fileinfo_t *
vgm_open2(uint32_t hints, DB_playItem_t *it) {
	vgm_info_t *info = vgm_open_int(hints);
	if (!info) {
		return NULL;
	}

	deadbeef->pl_lock();
		const char *uri = strdupa(deadbeef->pl_find_meta (it, ":URI"));
		int i = deadbeef->pl_find_meta_int(it, ":TRACKNUM", 0);
	deadbeef->pl_unlock();

	if (!i)
		i = 1;

	info->s = init_vgmstream_from_dbfile(i, uri);

	return (DB_fileinfo_t *) info;
}

static int vgm_init(DB_fileinfo_t *_info, DB_playItem_t *it) {
	vgm_info_t *info = (vgm_info_t *)_info;

	if (!info->s) {
		deadbeef->pl_lock();
			const char *uri = strdupa(deadbeef->pl_find_meta (it, ":URI"));
			int i = deadbeef->pl_find_meta_int(it, ":TRACKNUM", 0);
		deadbeef->pl_unlock();

		if (!i)
			i = 1;

		info->s = init_vgmstream_from_dbfile(i, uri);
		if(!info->s) return -1;
	}

	vgmstream_cfg_t vcfg = {0};

   	vcfg.allow_play_forever = 1;
   	vcfg.play_forever = conf_play_forever;
   	vcfg.loop_count = looptimes;
   	vcfg.fade_time = fadeseconds;
   	vcfg.fade_delay = fadedelayseconds;
   	vcfg.ignore_loop = 0;

   	vgmstream_apply_config(info->s, &vcfg);

	int input_channels = info->s->channels;
	int output_channels = info->s->channels;
	/* enable after all config but before outbuf */
	vgmstream_mixing_autodownmix(info->s, downmix_channels);
	vgmstream_mixing_enable(info->s, MIN_BUFFER_SIZE, &input_channels, &output_channels);

	info->length_samples = vgmstream_get_samples(info->s);

	info->input_channels = input_channels;
	info->output_channels = output_channels;
	_info->fmt.bps = 16;
	_info->fmt.channels = output_channels;
	_info->fmt.samplerate  = info->s->sample_rate;
	for (int i = 0; i < _info->fmt.channels; i++)
		_info->fmt.channelmask |= 1 << i;
	_info->readpos = 0;
	_info->plugin = &plugin;
	return 0;
}

static void vgm_free(DB_fileinfo_t *_info) {
	vgm_info_t *info = (vgm_info_t *)_info;
	close_vgmstream(info->s);
	if (info) free(info);
}

static int vgm_read(DB_fileinfo_t *_info, char *bytes, int size) {
	int i, j;
	vgm_info_t *info = (vgm_info_t *)_info;
	int32_t sample_count = size / (info->output_channels * sizeof(int16_t));
    
	int length_samples = info->length_samples;
	int playForever = info->can_loop && conf_play_forever;

	/* Past the end? Let deadbeef know. */
	if ((!playForever || !info->s->loop_flag) && info->position >= length_samples) return 0;

	/* Report true position */
	_info->readpos = (float)info->s->current_sample / (float)_info->fmt.samplerate;

	short buffer[MIN_BUFFER_SIZE * VGMSTREAM_MAX_CHANNELS];
	int max_buffer_samples = MIN_BUFFER_SIZE;

	while (sample_count > 0) {
		int to_do = max_buffer_samples;
		if (to_do > sample_count) {
			to_do = sample_count;
		}

		if (!playForever) {
			if (info->position >= length_samples)
				break;
			
			if (info->position + to_do >= length_samples)
				to_do = length_samples - info->position;
		}

		render_vgmstream(buffer, to_do, info->s);

		size_t bytes_to_copy = sizeof(short) * to_do * info->output_channels;
		memcpy(bytes, buffer, bytes_to_copy);
		bytes += bytes_to_copy;

		info->position += to_do;
		sample_count -= to_do;
	}

	if (sample_count > 0)
		size -= sample_count * sizeof(short) * info->output_channels;

	return size;
}

static int vgm_seek_sample(DB_fileinfo_t *_info, int sample) {
	int i;
	vgm_info_t *info = (vgm_info_t *)_info;
    
	if (info->s->loop_flag) {
		if (sample >= info->s->loop_start_sample) {
			sample -= info->s->loop_start_sample;
			sample %= (info->s->loop_end_sample - info->s->loop_start_sample);
			sample += info->s->loop_start_sample;
		}
	}

	seek_vgmstream(info->s, sample);
	info->position = sample;

	/* Report true position */
	_info->readpos = (float)info->s->current_sample / (float)_info->fmt.samplerate;

	return 0;
}

static int vgm_seek(DB_fileinfo_t *_info, float time) {
	return vgm_seek_sample(_info, time * _info->fmt.samplerate);
}

static int get_description_tag(char * temp, size_t temp_len, const char * description, const char * tag, char delimiter) {
	// extract a "tag" from the description string
	const char * pos = strstr(description, tag);
	const char * eos;
	size_t max_len = temp_len - 1;
	if (pos != NULL) {
		pos += strlen(tag);
		eos = strchr(pos, delimiter);
		if (eos == NULL) eos = pos + strlen(pos);
		if ((eos - pos) > max_len)
			eos = pos + max_len;
		strncpy(temp, pos, eos - pos);
		temp[eos - pos] = '\0';
		return 1;
	}
	return 0;
}

static int vgm_read_metadata(ddb_playItem_t *it) {
	char temp[2048];

	deadbeef->pl_lock ();
		const char *uri = strdupa(deadbeef->pl_find_meta(it, ":URI"));
		int i = deadbeef->pl_find_meta_int(it, ":TRACKNUM", 0);
	deadbeef->pl_unlock ();

	VGMSTREAM *infostream = init_vgmstream_from_dbfile(i, uri);
	if (!infostream)
		return -1;

	vgmstream_get_title(temp, sizeof(temp), uri, infostream, NULL);
	deadbeef->pl_add_meta(it, "title", temp);
		
	//todo improve string functions
	char tagfile_path[PATH_LIMIT];
	strcpy(tagfile_path, uri);

	char *path = strrchr(tagfile_path,'/');
	if (path != NULL) {
		path[1] = '\0';  /* includes "/", remove after that from tagfile_path */
		strcat(tagfile_path,tagfile_name);
	}
	else { /* ??? */
		strcpy(tagfile_path,tagfile_name);
	}

	STREAMFILE* sf_tags = dbsf_create_from_path(tagfile_path);
	if (sf_tags != NULL) {
		VGMSTREAM_TAGS* tags;
		const char *tag_key, *tag_val;

		tags = vgmstream_tags_init(&tag_key, &tag_val);
		vgmstream_tags_reset(tags, uri);
		while (vgmstream_tags_next_tag(tags, sf_tags)) {
			if (strcasecmp(tag_key, "ARTIST") == 0)
				deadbeef->pl_replace_meta(it, "artist", tag_val);
			else if (strcasecmp(tag_key, "ALBUMARTIST") == 0)
				deadbeef->pl_replace_meta(it, "albumartist", tag_val);
			else if (strcasecmp(tag_key, "TITLE") == 0)
				deadbeef->pl_replace_meta(it, "title", tag_val);
			else if (strcasecmp(tag_key, "ALBUM") == 0)
				deadbeef->pl_replace_meta(it, "album", tag_val);
			else if (strcasecmp(tag_key, "PERFORMER") == 0)
				deadbeef->pl_replace_meta(it, "performer", tag_val);
			else if (strcasecmp(tag_key, "COMPOSER") == 0)
				deadbeef->pl_replace_meta(it, "composer", tag_val);
			else if (strcasecmp(tag_key, "COMMENT") == 0)
				deadbeef->pl_replace_meta(it, "comment", tag_val);
			else if (strcasecmp(tag_key, "GENRE") == 0)
				deadbeef->pl_replace_meta(it, "genre", tag_val);
			else if (strcasecmp(tag_key, "TRACK") == 0)
				deadbeef->pl_replace_meta(it, "track", tag_val);
			else if (strcasecmp(tag_key, "YEAR") == 0)
				deadbeef->pl_replace_meta(it, "date", tag_val);
			else if (strcasecmp(tag_key, "REPLAYGAIN_TRACK_GAIN") == 0)
				deadbeef->pl_set_item_replaygain(it, DDB_REPLAYGAIN_TRACKGAIN, atof(tag_val));
			else if (strcasecmp(tag_key, "REPLAYGAIN_TRACK_PEAK") == 0)
				deadbeef->pl_set_item_replaygain(it, DDB_REPLAYGAIN_TRACKPEAK, atof(tag_val));
			else if (strcasecmp(tag_key, "REPLAYGAIN_ALBUM_GAIN") == 0)
				deadbeef->pl_set_item_replaygain(it, DDB_REPLAYGAIN_ALBUMGAIN, atof(tag_val));
			else if (strcasecmp(tag_key, "REPLAYGAIN_ALBUM_PEAK") == 0)
				deadbeef->pl_set_item_replaygain(it, DDB_REPLAYGAIN_ALBUMPEAK, atof(tag_val));
		}

		vgmstream_tags_close(tags);
		close_streamfile(sf_tags);
	}

	return 0;
}

static DB_playItem_t *vgm_insert_subsong(int subsong, ddb_playlist_t *plt, DB_playItem_t *after, const char *uri) {
	char temp[2048];
	char description[2048];

	DB_playItem_t *it = deadbeef->pl_item_alloc_init(uri, plugin.plugin.id);
	deadbeef->pl_set_meta_int (it, ":TRACKNUM", subsong);

	VGMSTREAM *infostream = init_vgmstream_from_dbfile(subsong, uri);

	vgmstream_cfg_t vcfg = {0};

	vcfg.allow_play_forever = 1;
	vcfg.play_forever = conf_play_forever;
	vcfg.loop_count = looptimes;
	vcfg.fade_time = fadeseconds;
	vcfg.fade_delay = fadedelayseconds;
	vcfg.ignore_loop = 0;

	vgmstream_apply_config(infostream, &vcfg);

	int output_channels = infostream->channels;
	vgmstream_mixing_autodownmix(infostream, downmix_channels);
	vgmstream_mixing_enable(infostream, 0, NULL, &output_channels);

	int sample_rate = infostream->sample_rate;
	int bitrate = get_vgmstream_average_bitrate(infostream);
	int length_samples = vgmstream_get_samples(infostream);
	double length_seconds = (double)length_samples/(double)sample_rate;
	describe_vgmstream(infostream, description, sizeof(description));

	if (get_description_tag(temp, sizeof(temp), description, "encoding: ", '\n'))
		deadbeef->pl_replace_meta(it, ":FILETYPE", temp);
	else
		deadbeef->pl_replace_meta(it, ":FILETYPE", "vgm");
	deadbeef->plt_set_item_duration(plt, it, length_seconds);

	if (infostream->loop_flag) {
		deadbeef->pl_set_meta_int(it, ":loop_start", infostream->loop_start_sample);
		deadbeef->pl_set_meta_int(it, ":loop_end", infostream->loop_end_sample);
	}

	snprintf(temp, sizeof(temp), "%d", sample_rate);
	deadbeef->pl_add_meta(it, ":SAMPLERATE", temp);
	snprintf(temp, sizeof(temp), "%d", output_channels);
	deadbeef->pl_add_meta(it, ":CHANNELS", temp);
	deadbeef->pl_add_meta(it, ":BPS", "16");
	snprintf(temp, sizeof(temp), "%d", bitrate / 1000);
	deadbeef->pl_add_meta(it, ":BITRATE", temp);

	close_vgmstream(infostream);

	vgm_read_metadata(it);

	after = deadbeef->plt_insert_item(plt, after, it);
	deadbeef->pl_item_unref (it);

	return after;
}

static DB_playItem_t *vgm_insert(ddb_playlist_t *plt, DB_playItem_t *after, const char *uri) {
	VGMSTREAM *infostream = init_vgmstream_from_dbfile(0, uri);
	if (!infostream) return after;

	int subsong = infostream->stream_index;

	if(subsong) {
		close_vgmstream(infostream);

		after = vgm_insert_subsong(subsong, plt, after, uri);
	} else {
		int total_subtunes = infostream->num_streams;
		if (!total_subtunes) {
			total_subtunes = 1;
		}

		close_vgmstream(infostream);

		for (int i = 1; i <= total_subtunes; ++i) {
			after = vgm_insert_subsong(i, plt, after, uri);
		}
	}

	return after;
}

static int vgm_start(void) {
	const char ** ext_list;
	size_t ext_list_len;
	size_t i;

	conf_play_forever = deadbeef->conf_get_int ("playback.loop", PLAYBACK_MODE_LOOP_ALL) == PLAYBACK_MODE_LOOP_SINGLE;

	ext_list = vgmstream_get_formats(&ext_list_len);

	plugin.exts = (const char **) calloc(sizeof(*plugin.exts), ext_list_len + 1);

	for (i = 0; i < ext_list_len; ++i) {
		plugin.exts[i] = ext_list[i];
	}

	plugin.exts[i] = NULL;

	return 0;
}

static int vgm_stop(void) {
	free(plugin.exts); plugin.exts = exts_placeholder;
	return 0;
}

int
vgm_message (uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2) {
	switch (id) {
		case DB_EV_CONFIGCHANGED:
			conf_play_forever = deadbeef->conf_get_int ("playback.loop", PLAYBACK_MODE_LOOP_ALL) == PLAYBACK_MODE_LOOP_SINGLE;
			break;
	}
	return 0;
}


// define plugin interface
static DB_decoder_t plugin = {
	DB_PLUGIN_SET_API_VERSION
	.plugin.version_major = 0,
	.plugin.version_minor = 1,
	.plugin.type = DB_PLUGIN_DECODER,
	.plugin.name = "vgmstream",
	.plugin.id = "vgm",
	.plugin.descr = "Decodes a variety of streaming video game music formats.",
	.plugin.copyright = COPYRIGHT_STR,
	.plugin.start = vgm_start,
	.plugin.stop = vgm_stop,
	.open = vgm_open,
	.open2 = vgm_open2,
	.init = vgm_init,
	.free = vgm_free,
	.read = vgm_read,
	.seek = vgm_seek,
	.seek_sample = vgm_seek_sample,
	.insert = vgm_insert,
	.read_metadata = vgm_read_metadata,
	.exts = exts_placeholder,
	.plugin.message = vgm_message,
};

__attribute__ ((visibility ("default")))
DB_plugin_t *vgm_load(DB_functions_t *api) {
	deadbeef = api;
	return DB_PLUGIN (&plugin);
}
